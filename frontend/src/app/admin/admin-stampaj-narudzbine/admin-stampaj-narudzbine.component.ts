import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Narudzbina } from 'src/app/model/narudzbina';

@Component({
  selector: 'app-admin-stampaj-narudzbine',
  templateUrl: './admin-stampaj-narudzbine.component.html',
  styleUrls: ['./admin-stampaj-narudzbine.component.css']
})
export class AdminStampajNarudzbineComponent implements OnInit {

  displayedColumns: string[] = ['nazivRestorana', 'datumDostave', 'nazivHrane', 'komentar', 'iznosZaNaplatu', 'moguceIsporuciti', 'odobreno'];
  dataSource = new MatTableDataSource<Narudzbina>();
  
  constructor() { }

  ngOnInit(): void {
    document.getElementById('app-header').style.display = 'none';
    let items = localStorage.getItem('stampa');
    this.dataSource.data = JSON.parse(items);
    setTimeout( 
      ()=>{
      window.print();
    }, 500)
  }

}
