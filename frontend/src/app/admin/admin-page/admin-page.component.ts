import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Narudzbina } from 'src/app/model/narudzbina';
import { LoginService } from 'src/app/service/login.service';
import { NarudzbinaService } from 'src/app/service/narudzbina.service';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['nazivRestorana', 'datumDostave', 'nazivHrane', 'komentar', 'iznosZaNaplatu', 'moguceIsporuciti', 'odobrena', 'odobri'];
  dataSource = new MatTableDataSource<Narudzbina>();
  filterParametar: string;
  filterJeKliknut = false;
  oldDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private narudzbinaService: NarudzbinaService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if (!this.loginService.daLiJeKorisnikUlogovan()) {
      this.router.navigate(['login']);
      return;
    }
    if (!this.loginService.ulogovanJeAdmin()) {
      this.router.navigate(['home']);
      return;
    }
    this.dajSveNarudzbine();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  dajSveNarudzbine() {
    this.narudzbinaService.getAll().subscribe(
      data => {
        console.log(data);
        this.dataSource.data = data;
        this.oldDataSource = data;
      },
      error => {
        console.log(error)
      }
    )
  }

  odobri(id: string) {
    this.narudzbinaService.odobriNarudzbinu(id).subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log('greska: ', error);
      }
    )
  }

  filtriraj() {
    if (!this.filterParametar || this.filterParametar === '' || this.filterParametar === ' ') {
      return;
    }
    this.filterJeKliknut = !this.filterJeKliknut;
    if (this.filterJeKliknut) {
      this.dataSource.data = this.filtrirajPoImenu(this.filterParametar);
    } else {
      this.dataSource.data = this.oldDataSource;
    }
  }

  filtrirajPoImenu(imeRestorana: string) {
    let data = [];
    let regex = new RegExp(imeRestorana.toLowerCase());
    for (let narudzbina of this.oldDataSource) {
      if (regex.test(narudzbina.nazivRestorana.toLowerCase())) {
        data.push(narudzbina);
      }
    }
    return data;
  }

  openDialog(text: string, height: string, width: string, action: boolean) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: width,
      height: height,
      data: text
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['home']);
    });
  }

  stampaj() {
    localStorage.setItem('stampa', JSON.stringify(this.dataSource.data));
    setTimeout(() => {
      this.router.navigate(['admin-stampaj-narudzbine'])
    }, 1000);
  }

}
