import { Component, OnInit } from '@angular/core';
import { SviKorisnici } from '../../model/svi-korisnici';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-korisnici-stampaj',
  templateUrl: './korisnici-stampaj.component.html',
  styleUrls: ['./korisnici-stampaj.component.css']
})
export class KorisniciStampajComponent implements OnInit {

  displayedColumns = ['id', 'korisnickoIme', 'ime', 'prezime', 'adresa', 'brojTelefona'];
  dataSource = new MatTableDataSource<SviKorisnici>();

  constructor() { }

  ngOnInit(): void {
    document.getElementById('app-header').style.display = 'none';
    let items = localStorage.getItem('korisnici-stampaj');
    this.dataSource.data = JSON.parse(items);
    setTimeout( 
      ()=>{
      window.print();
    }, 1000)
  }

}
