import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SviKorisnici } from 'src/app/model/svi-korisnici';
import { KorisnikService } from 'src/app/service/korisnik.service';
import { LoginService } from 'src/app/service/login.service';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';

@Component({
  selector: 'app-svi-korisnici',
  templateUrl: './svi-korisnici.component.html',
  styleUrls: ['./svi-korisnici.component.css']
})
export class SviKorisniciComponent implements OnInit {

  sviKorisnici: SviKorisnici[] = [];
  displayedColumns: string[] = ['id', 'korisnickoIme', 'ime', 'prezime', 'adresa', 'brojTelefona'];
  dataSource = new MatTableDataSource<SviKorisnici>();
  filterParametar: string;
  filterJeKliknut = false;
  oldDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private korisnikService: KorisnikService,
    private loginService: LoginService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {

    if (!this.loginService.daLiJeKorisnikUlogovan()) {
      this.router.navigate(['login']);
      return;
    }
    if (!this.loginService.ulogovanJeAdmin()) {
      this.router.navigate(['home']);
      return;
    }
    this.dajSveKorisnike();
  }

  izbrisiAdmina(sviKorisnici: SviKorisnici[]) {
    for (let i = 0; i < sviKorisnici.length; i++) {
      let korisnik = sviKorisnici[i];
      for (let j = 0; j < korisnik.roles.length; j++) {
        if (korisnik.roles[j]['type'] === "ROLE_ADMIN") {
          sviKorisnici.splice(i, 1);
        }
      }
    }
    this.oldDataSource = this.sviKorisnici;;
  }

  dajSveKorisnike() {
    this.korisnikService.dajSveKorisnike().subscribe(
      data => {
        this.sviKorisnici = data;
        this.izbrisiAdmina(this.sviKorisnici);
        this.dataSource.data = this.sviKorisnici;
      },
      error => {
        console.log(error);
      }
    )
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  filtrirajPoNazivuUlice() {
    if (!this.filterParametar || this.filterParametar === '' || this.filterParametar === ' ') {
      return;
    }
    this.filterJeKliknut = !this.filterJeKliknut;
    if (this.filterJeKliknut) {
      this.dataSource.data = this.filtrirajPoImenu(this.filterParametar);
    } else {
      this.dataSource.data = this.oldDataSource;
    }
  }

  filtrirajPoImenu(adresa: string) {
    let data = [];
    let regex = new RegExp(adresa.toLowerCase());
    for (let korisnik of this.oldDataSource) {
      if (regex.test(korisnik.adresa.toLowerCase())) {
        data.push(korisnik);
      }
    }
    return data;
  }

  openDialog(text: string, height: string, width: string, action: boolean) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: width,
      height: height,
      data: text
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['home']);
    });
  }

  stampaj() {
    localStorage.setItem('korisnici-stampaj', JSON.stringify(this.dataSource.data));
    setTimeout(() => {
      this.router.navigate(['korisnici-stampaj'])
    }, 1000);
  }


}
