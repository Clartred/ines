import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
import { SviKorisnici } from '../model/svi-korisnici';
import { Korisnik } from '../model/korisnik';

@Injectable({
  providedIn: 'root'
})
export class KorisnikService {

  apiUrl = 'http://localhost:8080/korisnik';

  constructor(
    private http: HttpClient,
    private loginService: LoginService
  ) { }

  dajSveKorisnike() {
    return this.http.get<SviKorisnici[]>(this.apiUrl, { headers: this.loginService.getHeaders() })
  }

  dajKorisnikaPoId(id: string) {
    return this.http.get<Korisnik>(this.apiUrl + '/' + id, { headers: this.loginService.getHeaders() });
  }

}
