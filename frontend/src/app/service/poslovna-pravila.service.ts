import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class PoslovnaPravilaService {

  apiUrl = 'http://localhost:8080/poslovna-pravila';

  constructor(
    private http: HttpClient,
    private loginService: LoginService
  ) { }

  dajMinimalnuCenu() {
    return this.http.get<number>(this.apiUrl + '/minimalna-cena', { headers: this.loginService.getHeaders() })
  }

  proveriDaLiJeAutomatskaStampaOdobrena(){
    return this.http.get<boolean>(this.apiUrl + '/racun-se-automatski-generise', { headers: this.loginService.getHeaders() })
  }

}
