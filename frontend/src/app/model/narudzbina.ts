export class Narudzbina {
    id: string;
    datumDostave: Date;
    nazivRestorana: string;
    nazivHrane: string;
    komentar: string;
    iznosZaNaplatu: number;
    idNarucioca: string;
    moguceIsporuciti: boolean;
    verifikovana: boolean;
}
