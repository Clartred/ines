export class Korisnik {

    id: string;
    ime: string;
    prezime: string;
    korisnickoIme: string;
    adresa: string;
    brojTelefona;
    sifra: string;
    email: string;
}
