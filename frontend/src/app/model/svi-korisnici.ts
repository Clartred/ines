export class SviKorisnici {

    id: string;
    korisnickoIme: string;
    ime: string;
    prezime: string;
    adresa: string;
    brojTelefona: string;
    roles: any[];

}
