export class Racun {
    nazivRestorana: string;
    imeKorisnika: string;
    prezimeKorisnika: string;
    adresa: string;
    broj: string;
    iznosZaNaplatu: string;
    datumDostave: Date;
    nazivObroka: string;
    vremeIzdavanjaRacuna: Date;

    constructor(
        nazivRestorana: string,
        imeKorisnika: string,
        prezimeKorisnika: string,
        adresa: string,
        broj: string,
        iznosZaNaplatu: string,
        datumDostave: Date,
        nazivObroka: string,
        vremeIzdavanjaRacuna: Date
    ) {
        this.nazivRestorana = nazivRestorana;
        this.imeKorisnika = imeKorisnika;
        this.prezimeKorisnika = prezimeKorisnika;
        this.adresa = adresa;
        this.broj = broj;
        this.iznosZaNaplatu = iznosZaNaplatu;
        this.datumDostave = datumDostave;
        this.nazivObroka = nazivObroka;
        this.vremeIzdavanjaRacuna = vremeIzdavanjaRacuna;
    }

}
