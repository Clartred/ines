import { Component, OnInit } from '@angular/core';
import { Racun } from '../model/racun';

@Component({
  selector: 'app-racun',
  templateUrl: './racun.component.html',
  styleUrls: ['./racun.component.css']
})
export class RacunComponent implements OnInit {

  racun: Racun;

  constructor() { }

  ngOnInit(): void {
    document.getElementById('app-header').style.display = 'none';
    let items = localStorage.getItem('racun');
    this.racun = JSON.parse(items);
    setTimeout(() => {
      window.print();
    }, 500)

  }

}
