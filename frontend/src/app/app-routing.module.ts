import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './shared/home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NaruciHranuComponent } from './naruci-hranu/naruci-hranu.component';
import { DodajRestoranComponent } from './dodaj-restoran/dodaj-restoran.component';
import { IzmeniNarudzbinuComponent } from './izmeni-narudzbinu/izmeni-narudzbinu.component';
import { StranicaZaStampuComponent } from './stranica-za-stampu/stranica-za-stampu.component';
import { AdminPageComponent } from './admin/admin-page/admin-page.component';
import { SviKorisniciComponent } from './admin/svi-korisnici/svi-korisnici.component';
import { AdminStampajNarudzbineComponent } from './admin/admin-stampaj-narudzbine/admin-stampaj-narudzbine.component';
import { KorisniciStampajComponent } from './admin/korisnici-stampaj/korisnici-stampaj.component';
import { RacunComponent } from './racun/racun.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'naruci-hranu', component: NaruciHranuComponent },
  { path: 'dodaj-restoran', component: DodajRestoranComponent },
  { path: 'izmeni-narudzbinu/:id', component: IzmeniNarudzbinuComponent },
  { path: 'stampaj', component: StranicaZaStampuComponent },
  { path: 'admin-page', component: AdminPageComponent },
  { path: 'svi-korisnici', component: SviKorisniciComponent },
  { path: 'admin-stampaj-narudzbine', component: AdminStampajNarudzbineComponent },
  { path: 'korisnici-stampaj', component: KorisniciStampajComponent },
  { path: 'stampaj-racun', component: RacunComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
