import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../shared/dialog/dialog.component';
import { Korisnik } from '../model/korisnik';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  REGEX_ZA_VALIDACIJU_EMAILA = /((([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,}))/i;
  SIFRA_IMA_MALA_SLOVA = /[a-z]/;
  SIFRA_IMA_VELIKA_SLOVA = /[A-Z]/;
  SIFRA_IMA_BROJ = /[0-9]/;
  SIFRA_IMA_SPECIJALNI_KARAKTER = /[!@#$%^&*()\[\],.?":{}|<>]/;

  korisnickoIme: string;
  sifra: string;
  ime: string;
  prezime: string;
  adresa: string;
  brojTelefona: string;
  ponovoSifra: string;
  email: string;

  constructor(
    private loginService: LoginService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if (this.loginService.daLiJeKorisnikUlogovan()) {
      this.router.navigate(['home']);
    }
  }

  validirajPolja() {

    //VALIDACIJA DA LI SU POLJA PRAZNA
    if (!this.korisnickoIme || this.korisnickoIme === '') {
      this.openDialog('Morate uneti korisnicko ime', '350px', '300px', false);
      return false;
    } else if (!this.ime || this.ime === '') {
      this.openDialog('Morate uneti ime', '350px', '300px', false);
      return false;
    } else if (!this.prezime || this.prezime === '') {
      this.openDialog('Morate uneti prezime', '350px', '300px', false);
      return false;
    } else if (!this.email || this.email === '') {
      this.openDialog('Morate uneti email adresu', '350px', '300px', false);
      return false;
    } else if (!this.adresa || this.adresa === '') {
      this.openDialog('Morate uneti adresu', '350px', '300px', false);
      return false;
    } else if (!this.brojTelefona || this.brojTelefona === '') {
      this.openDialog('Morate uneti broj telefona', '350px', '300px', false);
      return false;
    } else if (!this.sifra || this.sifra === '') {
      this.openDialog('Morate uneti sifru', '350px', '300px', false);
      return false;
    } else if (!this.ponovoSifra || this.ponovoSifra === '') {
      this.openDialog('Morate ponoviti sifru', '350px', '300px', false);
      return false;
    }

    if (this.sifra.length < 10) {
      this.openDialog('Sifra mora biti duza od 10 karaktera', '350px', '300px', false);
      return false;
    }

    if (!this.SIFRA_IMA_MALA_SLOVA.test(this.sifra)) {
      this.openDialog('Sifra mora imati bar jedno malo slovo', '350px', '300px', false);
      return false;
    }

    if (!this.SIFRA_IMA_VELIKA_SLOVA.test(this.sifra)) {
      this.openDialog('Sifra mora imati bar jedno veliko slovo', '350px', '300px', false);
      return false;
    }

    if (!this.SIFRA_IMA_BROJ.test(this.sifra)) {
      this.openDialog('Sifra mora imati bar jedan broj', '350px', '300px', false);
      return false;
    }

    if (!this.SIFRA_IMA_SPECIJALNI_KARAKTER.test(this.sifra)) {
      this.openDialog('Sifra mora imati bar jedan specijalni karakter', '350px', '300px', false);
      return false;
    }

    if (this.ponovoSifra !== this.sifra) {
      this.openDialog('Sifre se moraju poklapati', '350px', '300px', false);
      return false;
    }

    if (!this.REGEX_ZA_VALIDACIJU_EMAILA.test(this.email)) {
      this.openDialog('Email mora biti ispravnog formata', '350px', '300px', false);
      return false;
    }

    //VALIDACIJA DA LI SU POLJA ODGOVARAJUCE DUZINE
    if (this.korisnickoIme.length > 30) {
      this.openDialog('Korisnicko ime ne moze imati vise od 30 karaktera', '350px', '300px', false);
      return false;
    }

    if (this.ime.length > 30) {
      this.openDialog('Ime ne moze imati vise od 30 karaktera', '350px', '300px', false);
      return false;
    }

    if (this.prezime.length > 30) {
      this.openDialog('Prezime ne moze imati vise od 30 karaktera', '350px', '300px', false);
      return false;
    }

    if (this.adresa.length > 50) {
      this.openDialog('Adresa ne moze imati vise od 50 karaktera', '350px', '300px', false);
      return false;
    }

    if (this.brojTelefona.length > 16) {
      this.openDialog('Broj telefona ne moze imati vise od 16 karaktera', '350px', '300px', false);
      return false;
    }

    if (this.brojTelefona[0] !== '+' || this.brojTelefona[1] !== '3' || this.brojTelefona[2] !== '8' || this.brojTelefona[3] !== '1') {
      this.openDialog('Broj telefona mora poceti sa +381', '350px', '300px', false);
      return false;
    }

    return true;
  }

  registrujSe() {

    if (this.validirajPolja()) {

      this.register();
    }
  }

  register() {
    let korisnik = new Korisnik();
    korisnik.ime = this.ime;
    korisnik.korisnickoIme = this.korisnickoIme;
    korisnik.prezime = this.prezime;
    korisnik.adresa = this.adresa;
    korisnik.brojTelefona = this.brojTelefona;
    korisnik.sifra = this.sifra;
    korisnik.email = this.email;

    this.loginService.register(korisnik).subscribe(
      data => {
        this.openDialog('Uspesno ste se registrovali! \n Ulogujte se! ', '350px', '300px', true);
      },
      error => {
        console.log('error: ', error);
        if (JSON.stringify(error).includes('500')) {
          this.openDialog('Korisnicko ime je zauzeto', '350px', '300px', false);
        } else {
          this.openDialog('Postoji greska pri registraciji!', '350px', '300px', false);
        }
      }
    );
  }

  openDialog(text: string, height: string, width: string, action: boolean) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: width,
      height: height,
      data: text
    });

    dialogRef.afterClosed().subscribe(result => {
      if (action) {
        this.router.navigate(['login']);
      }
    });
  }

}
