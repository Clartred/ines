import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../shared/dialog/dialog.component';
import { Narudzbina } from '../model/narudzbina';
import { NarudzbinaService } from '../service/narudzbina.service';
import { LoginService } from '../service/login.service';
import { RestoranService } from '../service/restoran.service';
import { Restoran } from '../model/restoran';
import { PoslovnaPravilaService } from '../service/poslovna-pravila.service';
import { Racun } from '../model/racun';
import { KorisnikService } from '../service/korisnik.service';
import { Korisnik } from '../model/korisnik';

@Component({
  selector: 'app-naruci-hranu',
  templateUrl: './naruci-hranu.component.html',
  styleUrls: ['./naruci-hranu.component.css']
})
export class NaruciHranuComponent implements OnInit {

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private narudzbinaService: NarudzbinaService,
    private loginService: LoginService,
    private restoranService: RestoranService,
    private poslovnaPravila: PoslovnaPravilaService,
    private korisnikService: KorisnikService
  ) { }

  narudzbina: Narudzbina;
  datumDostave: Date;
  nazivRestorana: string;
  nazivHrane: string;
  komentar: string;
  iznosZaNaplatu: number;
  minimalniDatum: Date;
  minimalnaCena: number;

  restorani: Restoran[];

  ngOnInit(): void {
    if (!this.loginService.getHeaders()) {
      this.router.navigate(['home']);
      return;
    }
    this.dajSveRestorane();
    this.postaviMinimalniDatum();
    this.dajMinimalnuCenu();
  }

  dajMinimalnuCenu() {
    this.poslovnaPravila.dajMinimalnuCenu().subscribe(
      data => {
        this.minimalnaCena = data;
      },
      error => {
        console.log('error: ', error);
      }
    )
  }

  dajSveRestorane() {
    this.restoranService.dajSveRestorane().subscribe(
      data => {
        this.restorani = data;
        console.log(data);
      },
      error => {
        console.log(error);
      }
    )
  }

  postaviMinimalniDatum() {
    this.minimalniDatum = new Date();
  }

  naruci() {
    if (!this.nazivRestorana || this.nazivRestorana === '') {
      this.openDialog('Morate uneti naziv restorana', '350px', '300px', false);
      return;
    } else if (!this.nazivHrane || this.nazivHrane === '') {
      this.openDialog('Morate uneti naziv hrane', '350px', '300px', false);
      return;
    } else if (!this.komentar || this.komentar === '') {
      this.openDialog('Morate uneti komentar', '350px', '300px', false);
      return;
    } else if (!this.iznosZaNaplatu) {
      this.openDialog('Morate uneti iznos za naplatu', '350px', '300px', false);
      return;
    } else if (!this.datumDostave) {
      this.openDialog('Morate uneti datum dostave', '350px', '300px', false);
      return;
    }

    if (this.iznosZaNaplatu < 1) {
      this.openDialog('Iznos za naplatu ne moze biti negativan', '350px', '300px', false);
      return;
    }

    if (this.iznosZaNaplatu < 50) {
      this.openDialog('Iznos za naplatu ne moze biti manji od 50 dinara', '350px', '300px', false);
      return;
    }

    let narudzbina = new Narudzbina();
    narudzbina.datumDostave = this.datumDostave;
    narudzbina.nazivHrane = this.nazivHrane;
    narudzbina.nazivRestorana = this.nazivRestorana;
    narudzbina.komentar = this.komentar;
    narudzbina.iznosZaNaplatu = this.iznosZaNaplatu;
    narudzbina.idNarucioca = this.loginService.dajKorisnika().id;
    narudzbina.verifikovana = false;
    console.log(JSON.stringify(narudzbina));
    this.naruciHranu(narudzbina);
  }

  naruciHranu(narudzbina: Narudzbina) {

    this.narudzbinaService.save(narudzbina).subscribe(
      data => {
        this.narudzbina = data;
        if (data.moguceIsporuciti) {
          this.proveriDaLiJeAutomatskaStampaOdobrena();
        } else {
          this.openDialog('Iznos mora biti ' + (this.minimalnaCena + 1) + ' ili više dinara.', '350px', '300px', false);
          this.router.navigate(['home']);
        }
      },
      error => {
        this.openDialog('Doslo je do greske', '350px', '300px', false);
      })
  }

  proveriDaLiJeAutomatskaStampaOdobrena() {
    this.poslovnaPravila.proveriDaLiJeAutomatskaStampaOdobrena().subscribe(
      data => {
        if (data) {
          this.openDialog('Uspesno ste narucili hranu. Racun ce se automatski izgeneratisati', '380px', '400px', true);

        } else {
          this.openDialog('Uspesno ste narucili hranu', '350px', '300px', false);
        }
      },
      error => {
        console.log('error: ', error);
      }
    )
  }

  openDialog(text: string, height: string, width: string, action: boolean) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: width,
      height: height,
      data: text
    });

    dialogRef.afterClosed().subscribe(result => {
      if (action) {
        this.dajKorisnikaPoId(this.narudzbina.idNarucioca);
      }
    });
  }

  dajKorisnikaPoId(id: string) {
    this.korisnikService.dajKorisnikaPoId(id).subscribe(
      data => {
        this.stampaj(data);
      },
      error => {
        console.log('error: ', error);
      }
    )
  }

  stampaj(korisnik: Korisnik) {
    console.log('this.narudzbina.iznosZaNaplatu: ', this.narudzbina.iznosZaNaplatu);
    let racun = new Racun(
      this.narudzbina.nazivRestorana,
      korisnik.ime,
      korisnik.prezime,
      korisnik.adresa,
      korisnik.brojTelefona,
      this.narudzbina.iznosZaNaplatu + '',
      this.narudzbina.datumDostave,
      this.narudzbina.nazivHrane,
      new Date()
    );
    localStorage.setItem('racun', JSON.stringify(racun));
    setTimeout(() => {
      this.router.navigate(['stampaj-racun'])
    }, 700);
  }


}
