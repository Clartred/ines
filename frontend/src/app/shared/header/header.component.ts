import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public korisnikJeUlogovan = false;
  korisnikJeAdmin = false;

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.loginService.daLiJeKorisnikUlogovan()) {
      this.korisnikJeUlogovan = true;
      this.router.navigate(['home']);
    } else {
      this.korisnikJeUlogovan = false;
    }
    if(this.loginService.ulogovanJeAdmin()){
      this.korisnikJeAdmin = true;
    }
    this.gledajPromeneNaRuti();
  }

  gledajPromeneNaRuti() {
    setInterval(() => {
      this.router.events.subscribe(() => {
        if(this.loginService.ulogovanJeAdmin()){
          this.korisnikJeAdmin = true;
        }
        this.korisnikJeUlogovan = this.loginService.daLiJeKorisnikUlogovan();
      });
    }, 2500)

  }

  ulogujSe() {
    this.router.navigate(['login']);
  }

  izlogujSe() {
    this.loginService.logout();
  }

  redirectToHome(){
    this.router.navigate(['home']);
  }

  redirektNaSveKorisnike(){
    this.router.navigate(['svi-korisnici']);
  }

}
