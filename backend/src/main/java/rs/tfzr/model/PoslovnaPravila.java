package rs.tfzr.model;

public class PoslovnaPravila {

    private int minimalnaCena;
    private int brojNarudzbinaZaOstvarivanjePopusta;
    private int procenatPopusta;
    private boolean automatskiSeGeneriseRacun;

    public PoslovnaPravila() {
    }

    public PoslovnaPravila(int minimalnaCena, int brojNarudzbinaZaOstvarivanjePopusta, int procenatPopusta, boolean automatskiSeGeneriseRacun) {
        this.minimalnaCena = minimalnaCena;
        this.brojNarudzbinaZaOstvarivanjePopusta = brojNarudzbinaZaOstvarivanjePopusta;
        this.procenatPopusta = procenatPopusta;
        this.automatskiSeGeneriseRacun = automatskiSeGeneriseRacun;
    }

    public int getMinimalnaCena() {
        return minimalnaCena;
    }

    public void setMinimalnaCena(int minimalnaCena) {
        this.minimalnaCena = minimalnaCena;
    }

    public int getBrojNarudzbinaZaOstvarivanjePopusta() {
        return brojNarudzbinaZaOstvarivanjePopusta;
    }

    public void setBrojNarudzbinaZaOstvarivanjePopusta(int brojNarudzbinaZaOstvarivanjePopusta) {
        this.brojNarudzbinaZaOstvarivanjePopusta = brojNarudzbinaZaOstvarivanjePopusta;
    }

    public int getProcenatPopusta() {
        return procenatPopusta;
    }

    public void setProcenatPopusta(int procenatPopusta) {
        this.procenatPopusta = procenatPopusta;
    }

    public boolean isAutomatskiSeGeneriseRacun() {
        return automatskiSeGeneriseRacun;
    }

    public void setAutomatskiSeGeneriseRacun(boolean automatskiSeGeneriseRacun) {
        this.automatskiSeGeneriseRacun = automatskiSeGeneriseRacun;
    }
}
