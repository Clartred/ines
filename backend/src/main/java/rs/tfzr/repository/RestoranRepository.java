package rs.tfzr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.tfzr.model.Restoran;

public interface RestoranRepository extends JpaRepository<Restoran, Long> {
}
