package rs.tfzr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.tfzr.model.Narudzbina;

import java.util.List;

public interface NarudzbinaRepository extends JpaRepository<Narudzbina, Long> {

    List<Narudzbina> findNarudzbinaByIdNarucioca(Long id);
}
