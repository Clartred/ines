package rs.tfzr.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import rs.tfzr.model.PoslovnaPravila;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

@Transactional
@Service
public class PoslovnaPravilaService {

    public int dajMinimalnuCenu() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            InputStream inputStream =  new ClassPathResource("json/POSLOVNA_PRAVILA.json").getInputStream();
            TypeReference<PoslovnaPravila> typeReference = new TypeReference<PoslovnaPravila>() {};
            PoslovnaPravila poslovnaPravila = mapper.readValue(inputStream, typeReference);
            return poslovnaPravila.getMinimalnaCena();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int dajBrojNarudzbinaZaOstvarivanjePopusta() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            InputStream inputStream =  new ClassPathResource("json/POSLOVNA_PRAVILA.json").getInputStream();
            TypeReference<PoslovnaPravila> typeReference = new TypeReference<PoslovnaPravila>() {};
            PoslovnaPravila poslovnaPravila = mapper.readValue(inputStream, typeReference);
            return poslovnaPravila.getBrojNarudzbinaZaOstvarivanjePopusta();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int dajprocenatPopusta() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            InputStream inputStream =  new ClassPathResource("json/POSLOVNA_PRAVILA.json").getInputStream();
            TypeReference<PoslovnaPravila> typeReference = new TypeReference<PoslovnaPravila>() {};
            PoslovnaPravila poslovnaPravila = mapper.readValue(inputStream, typeReference);
            return poslovnaPravila.getProcenatPopusta();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean dajautomatskiSeGeneriseRacun() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            InputStream inputStream =  new ClassPathResource("json/POSLOVNA_PRAVILA.json").getInputStream();
            TypeReference<PoslovnaPravila> typeReference = new TypeReference<PoslovnaPravila>() {};
            PoslovnaPravila poslovnaPravila = mapper.readValue(inputStream, typeReference);
            return poslovnaPravila.isAutomatskiSeGeneriseRacun();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}
