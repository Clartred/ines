package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Rola;
import rs.tfzr.repository.RolaRepository;

import javax.transaction.Transactional;

@Transactional
@Service
public class RolaService {

    private RolaRepository rolaRepository;

    @Autowired
    public RolaService(RolaRepository rolaRepository) {
        this.rolaRepository = rolaRepository;
    }

    public Rola getOne(Long id){
        return this.rolaRepository.getOne(id);
    }
}
