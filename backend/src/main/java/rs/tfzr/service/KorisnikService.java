package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Korisnik;
import rs.tfzr.model.Rola;
import rs.tfzr.repository.KorisnikRepository;
import rs.tfzr.repository.RolaRepository;

import javax.management.relation.Role;
import javax.transaction.Transactional;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Transactional
@Service
public class KorisnikService {

    private KorisnikRepository korisnikRepository;
    private RolaRepository rolaRepository;

    @Autowired
    public KorisnikService(KorisnikRepository korisnikRepository, RolaRepository rolaRepository) {
        this.korisnikRepository = korisnikRepository;
        this.rolaRepository = rolaRepository;
    }

    public UserDetails loadUserByUsername(String korisnickoIme) throws UsernameNotFoundException {
        try {
            Korisnik korisnik = korisnikRepository.findByKorisnickoIme(korisnickoIme);
            if (korisnik == null) {
                return null;
            }
            return new User(korisnik.getKorisnickoIme(), korisnik.getSifra(), getAuthorities(korisnik));
        } catch (Exception e) {
            throw new UsernameNotFoundException("User not found");
        }
    }

    private Set<GrantedAuthority> getAuthorities(Korisnik korisnik) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        System.out.println(korisnik.getRoles());

        for (Rola rola : korisnik.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(rola.getType().toString());
            authorities.add(grantedAuthority);
        }
        return authorities;
    }

    public List<Korisnik> findAll() {
        return korisnikRepository.findAll();
    }

    public Korisnik registrujSe(Korisnik korisnik) {
        return korisnikRepository.save(korisnik);
    }

    public Korisnik dajPoId(Long id) {
        return korisnikRepository.getOne(id);
    }

}
