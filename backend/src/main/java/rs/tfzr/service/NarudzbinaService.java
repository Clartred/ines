package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Narudzbina;
import rs.tfzr.repository.NarudzbinaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class NarudzbinaService {

    private NarudzbinaRepository narudzbinaRepository;
    private PoslovnaPravilaService poslovnaPravilaService;

    @Autowired
    public NarudzbinaService(NarudzbinaRepository narudzbinaRepository, PoslovnaPravilaService poslovnaPravilaService) {
        this.narudzbinaRepository = narudzbinaRepository;
        this.poslovnaPravilaService = poslovnaPravilaService;
    }

    public Narudzbina insert(Narudzbina narudzbina) {
        if (narudzbina.getIznosZaNaplatu() > poslovnaPravilaService.dajMinimalnuCenu()) {
            narudzbina.setMoguceIsporuciti(true);
        } else {
            narudzbina.setMoguceIsporuciti(false);
        }
        int brojPorudzbinaKorisnika = brojNarudzbinaKorisnika(narudzbina.getIdNarucioca());
        if (brojPorudzbinaKorisnika >= poslovnaPravilaService.dajBrojNarudzbinaZaOstvarivanjePopusta()) {
            System.out.println("Korisnik ima prava na popust");
            int procenatPopusta = poslovnaPravilaService.dajprocenatPopusta();
            Double cena = narudzbina.getIznosZaNaplatu();
            System.out.println("Korisnik je ostvario popust od " + procenatPopusta + "%");
            cena = cena - (cena / procenatPopusta);
            System.out.println("Cena je bila " + narudzbina.getIznosZaNaplatu() + " dinara a sada je " + cena + " dinara.");
            narudzbina.setIznosZaNaplatu(cena);
        }
        return narudzbinaRepository.save(narudzbina);
    }

    public Narudzbina getOne(Long id) {
        return narudzbinaRepository.getOne(id);
    }

    public List<Narudzbina> getAll() {
        return narudzbinaRepository.findAll();
    }

    public void delete(Long id) {
        narudzbinaRepository.deleteById(id);
    }

    public Narudzbina edit(Narudzbina narudzbina) {
        if (narudzbina.getIznosZaNaplatu() > poslovnaPravilaService.dajMinimalnuCenu()) {
            narudzbina.setMoguceIsporuciti(true);
        } else {
            narudzbina.setMoguceIsporuciti(false);
        }
        return narudzbinaRepository.save(narudzbina);
    }

    public List<Narudzbina> dajNarudzbinePoIdKorisnika(Long id) {
        return this.narudzbinaRepository.findNarudzbinaByIdNarucioca(id);
    }

    public boolean odobri(Long id) {
        Narudzbina narudzbina = this.narudzbinaRepository.getOne(id);
        narudzbina.setVerifikovana(true);
        narudzbinaRepository.save(narudzbina);
        return true;
    }

    public int brojNarudzbinaKorisnika(Long id) {
        return narudzbinaRepository.findNarudzbinaByIdNarucioca(id).size();
    }

}
