package rs.tfzr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.tfzr.model.AutentifikovaniKorisnik;
import rs.tfzr.model.Korisnik;
import rs.tfzr.model.Rola;
import rs.tfzr.repository.KorisnikRepository;
import rs.tfzr.service.RolaService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class LoginController {

    private KorisnikRepository korisnikRepository;
    private RolaService rolaService;

    @Autowired
    public LoginController(KorisnikRepository korisnik, RolaService rolaService) {
        this.korisnikRepository = korisnik;
        this.rolaService = rolaService;
    }

    @RequestMapping("/user")
    public AutentifikovaniKorisnik getUser(Authentication authentication) {
        System.out.println("login");
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            roles.add(authority.getAuthority());
        }

        Korisnik trenutniKorisnik = korisnikRepository.findByKorisnickoIme(authentication.getName());
        AutentifikovaniKorisnik korisnik = new AutentifikovaniKorisnik(
                trenutniKorisnik.getId(), authentication.getName(), roles);

        if (korisnik.getRoles().size() == 0) {
            korisnik.getRoles().add("USER");
        }

        return korisnik;
    }

}
