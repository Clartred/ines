package rs.tfzr.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.tfzr.service.PoslovnaPravilaService;

@RestController
@RequestMapping("/poslovna-pravila")
@CrossOrigin
public class PoslovnaPravilaController {

    private PoslovnaPravilaService poslovnaPravilaService;

    @Autowired
    public PoslovnaPravilaController(PoslovnaPravilaService poslovnaPravilaService) {
        this.poslovnaPravilaService = poslovnaPravilaService;
    }

    @GetMapping("/minimalna-cena")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity dajMinimalnuCenu() {
        return new ResponseEntity(this.poslovnaPravilaService.dajMinimalnuCenu(), HttpStatus.OK);
    }

    @GetMapping("/racun-se-automatski-generise")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity daLiSeRacunAutomtaskiGenerise() {
        return new ResponseEntity(this.poslovnaPravilaService.dajautomatskiSeGeneriseRacun(), HttpStatus.OK);
    }

}
