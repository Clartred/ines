package rs.tfzr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.tfzr.model.Restoran;
import rs.tfzr.service.RestoranService;

@RestController
@RequestMapping("/restoran")
@CrossOrigin
public class RestoranController {

    private RestoranService restoranService;

    @Autowired
    public RestoranController(RestoranService restoranService) {
        this.restoranService = restoranService;
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getAll() {
        return new ResponseEntity(this.restoranService.dajSve(), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity saveOne(@RequestBody Restoran restoran) {
        return new ResponseEntity(this.restoranService.dodajRestoran(restoran), HttpStatus.OK);
    }

}
