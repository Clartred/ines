
INSERT INTO `rola`(`id`, `type`) VALUES (1, 'ROLE_ADMIN');
INSERT INTO `rola`(`id`,`type`) VALUES (2, 'ROLE_USER');

INSERT INTO `korisnik`(`id`, `sifra`,`korisnicko_ime`, `ime`, `prezime`, `adresa`, `broj_telefona`, `email`) VALUES (3, 'i.karadjenov.admin','karadjenov.i-ines', 'Ines','Karadjenov', 'Augusta Cesarca 16', '+381647982556', 'i.karadjenov@gmail.com');

INSERT INTO `korisnik_roles`(`user_id`,`role_id`)VALUES(3, 1);

INSERT INTO `restoran`(`id`,`naziv`)VALUES(77, 'Camel');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(78, 'Domacin');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(79, 'Arena ZR');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(80, 'Kafemat');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(81, 'Lion Pub');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(82, 'Zanatlija');
